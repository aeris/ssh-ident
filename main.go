package main

import (
	"io/ioutil"
	"os"
	"os/user"
	"path"
	"strings"

	"gopkg.in/yaml.v2"
)

type Config struct {
	BinDir          string `yaml:"bin_dir"`
	AgentsDir       string
	IdentitiesDir   string
	DefaultIdentity string `yaml:"default_identity"`
	Identities      yaml.MapSlice
}

var sshOptions = map[string]map[string]string{
	"ssh": {
		"short": "1246AaconfiggKkMNnqsTtVvXxYy",
		"long":  "bcDEeFIiJLlmOopQRSWw",
	},
	"scp": {
		"short": "12346BCpqrv",
		"long":  "cFiloPS",
	},
}

func removeOptions(prog string, args []string) []string {
	notOptions := []string{}
	longOptions := sshOptions[prog]["long"]
	long := false
	for _, arg := range args {
		if long {
			long = false
			continue
		} else if strings.HasPrefix(arg, "-") {
			last := arg[len(arg)-1:]
			long = strings.Contains(longOptions, last)
		} else {
			notOptions = append(notOptions, arg)
		}
	}
	return notOptions
}

func main() {
	usr, err := user.Current()
	fatal(err)
	home := usr.HomeDir
	sshDir := path.Join(home, ".ssh")
	identitiesDir := path.Join(sshDir, "identities")

	configFile := path.Join(identitiesDir, "config.yml")
	data, err := ioutil.ReadFile(configFile)
	fatal(err)
	config := Config{}
	err = yaml.Unmarshal(data, &config)
	fatal(err)

	config.IdentitiesDir = identitiesDir

	prog := os.Getenv("SSH_BINARY")
	if prog == "" {
		prog = os.Args[0]
		prog = path.Base(prog)
	}
	args := os.Args[1:]

	identity := FindIdentity(config, prog, args)
	agentsDir := path.Join(sshDir, "agents")
	config.AgentsDir = agentsDir

	agent := NewAgent(config, identity)
	agent.Run(config, prog, args)
}
